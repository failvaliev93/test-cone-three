## Расчет врешин, индексов, нормалей и UV-развертки для параметризированного конуса.

Параметрами задаются высота, радиус основания и радиальный сегмент. 3д конус рисуется исходя их параметров и расчета "триангуляции".

В git ветке master расчет происходит на клиенте, в ветке withserver - на сервере. Локально express сервер запускается "npm run sstart", потом локально можно запустить проект "npm start".

Пример проекта с расчетом на клиенте: https://test-cone-three.vercel.app/

Основные технологии:
- [Create React App](https://create-react-app.dev/)
- [React Three Fiber](https://docs.pmnd.rs/react-three-fiber)
- [ThreeJs](https://threejs.org/)