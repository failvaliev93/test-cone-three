const express = require('express');
const coneRouters = require("./cone/cone.routers");
const cors = require("cors");

const PORT = process.env.PORT || 8811;

const server = express();
server.use(express.json({ limit: '50mb' }));

server.options("*", cors({ origin: 'http://localhost:3000', optionsSuccessStatus: 200 }));
server.use(cors({ origin: "http://localhost:3000", optionsSuccessStatus: 200 }));

server.use("/cone", coneRouters);

const start = () => {
  try {
    server.listen(PORT, () => {
      console.log("Server is running on port " + PORT);
    })
  } catch (error) {
    console.log("Server error");
    console.log(error);
  }
}

start();
