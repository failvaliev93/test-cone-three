const coneService = require('./cone.service');

class ConeController {
  /**
   * @param {*} req { height = 1, radius = 1, segments = 8 } 
   * @returns : {data: {vertices[], indices[], normals[], uvs[]}, status: 1}
   */
  async calc(req, res) {
    try {
      const { height, radius, segments } = req.body;

      if (!height || !radius || !segments) {
        return res.status(400).send({ message: "Parameters missing", status: 0 });
      }

      const vertices = [], indices = [], normals = [], uvs = [];

      coneService.calc(vertices, indices, normals, uvs, height, radius, segments);

      res.json({
        data: {
          vertices,
          indices,
          normals,
          uvs,
        },
        status: 1,
      });

    } catch (e) {
      console.log(e);
      res.status(400).send({ message: "Internal error", status: 0 });
    }
  }
}

module.exports = new ConeController();