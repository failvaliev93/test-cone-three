const express = require('express');
const coneController = require('./cone.controller');

const router = express.Router();

router.post('/', coneController.calc);

module.exports = router;