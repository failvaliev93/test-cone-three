import { Canvas } from '@react-three/fiber';
import { OrbitControls, PerformanceMonitor } from "@react-three/drei";
import React, { Suspense } from 'react';
import { Controlls } from './components/Controlls';

export const App = () => {
  return (
    <Suspense fallback={'Что-то не то случилось...'}>
      <Canvas
        shadows
        dpr={[1, 2]}
        camera={{ position: [2, 4, 2], near: 0.1, far: 10000 }}
        performance={{ min: 0.2 }}
      >
        <PerformanceMonitor flipflops={3}>
          <ambientLight color={0xffffff} intensity={0.2} />
          <directionalLight
            color="white"
            position={[0, 8, 0]}
            intensity={1}
          />
          <OrbitControls
            makeDefault
            target={[0, 1.65, 0]}
            enableDamping={false}
          />
          <Controlls />
        </PerformanceMonitor>
      </Canvas>
    </Suspense>
  );
}