import { GridHelper as ThreeGridHelper } from 'three';
import { extend, Object3DNode } from '@react-three/fiber';

extend({ ThreeGridHelper });

declare module '@react-three/fiber' {
  interface ThreeElements {
    gridHelper: Object3DNode<ThreeGridHelper, typeof ThreeGridHelper>
  }
}

export const GridHelper = () => {
  return <gridHelper />
}