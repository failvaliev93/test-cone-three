import { Vector2, Vector3 } from "three";

export function calc(vertices: number[], indices: number[], normals: number[], uvs: number[], height: number, radius: number, segments: number) {
  const normal = new Vector3();
  const vertex = new Vector3();
  const uv = new Vector2();

  // helper variables
  let index = 0;
  const indexArray = [];

  const heightSegments = 1;
  const halfHeight = height / 2;
  const radiusBottom = radius, radiusTop = 0;
  const radialSegments = segments;
  const thetaStart = 0, thetaLength = Math.PI * 2;

  // this will be used to calculate the normal
  const slope = (radiusBottom - radiusTop) / height;

  //generateTorso
  // generate vertices, normals and uvs
  for (let y = 0; y <= heightSegments; y++) {
    const indexRow = [];

    const v = y / heightSegments;

    // calculate the radius of the current row
    const radius = v * (radiusBottom - radiusTop) + radiusTop;

    for (let x = 0; x <= radialSegments; x++) {
      const u = x / radialSegments;
      const theta = u * thetaLength + thetaStart;

      const sinTheta = Math.sin(theta);
      const cosTheta = Math.cos(theta);

      // vertex
      vertex.x = radius * sinTheta;
      vertex.y = - v * height + halfHeight;
      vertex.z = radius * cosTheta;
      vertices.push(vertex.x, vertex.y, vertex.z);

      // normal
      normal.set(sinTheta, slope, cosTheta).normalize();
      normals.push(normal.x, normal.y, normal.z);

      // uv
      uvs.push(u, 1 - v);

      // save index of vertex in respective row
      indexRow.push(index++);
    }

    // now save vertices of the row in our index array
    indexArray.push(indexRow);
  }

  // generate indices
  for (let x = 0; x < radialSegments; x++) {
    for (let y = 0; y < heightSegments; y++) {
      // we use the index array to access the correct indices
      const a = indexArray[y][x];
      const b = indexArray[y + 1][x];
      const c = indexArray[y + 1][x + 1];
      const d = indexArray[y][x + 1];

      // faces
      indices.push(a, b, d);
      indices.push(b, c, d);
    }

  }


  //CAP
  // save the index of the first center vertex
  const centerIndexStart = index;

  const sign = - 1;

  // first we generate the center vertex data of the cap.
  // because the geometry needs one set of uvs per face,
  // we must generate a center vertex per face/segment
  for (let x = 1; x <= radialSegments; x++) {
    vertices.push(0, halfHeight * sign, 0);
    normals.push(0, sign, 0);
    uvs.push(0.5, 0.5);
    index++;
  }

  // save the index of the last center vertex
  const centerIndexEnd = index;

  // now we generate the surrounding vertices, normals and uvs
  for (let x = 0; x <= radialSegments; x++) {
    const u = x / radialSegments;
    const theta = u * thetaLength + thetaStart;

    const cosTheta = Math.cos(theta);
    const sinTheta = Math.sin(theta);

    // vertex
    vertex.x = radius * sinTheta;
    vertex.y = halfHeight * sign;
    vertex.z = radius * cosTheta;
    vertices.push(vertex.x, vertex.y, vertex.z);

    // normal
    normals.push(0, sign, 0);

    // uv
    uv.x = (cosTheta * 0.5) + 0.5;
    uv.y = (sinTheta * 0.5 * sign) + 0.5;
    uvs.push(uv.x, uv.y);

    index++;
  }

  // generate indices
  for (let x = 0; x < radialSegments; x++) {
    const c = centerIndexStart + x;
    const i = centerIndexEnd + x;
    indices.push(i + 1, i, c);
  }
}