import { BufferGeometry, Float32BufferAttribute } from 'three';
import { useRef, useEffect } from 'react';

interface GeometryProps {
  wireframe: boolean;
  vertices: number[];
  indices: number[];
  normals: number[];
  uvs: number[];
}

export const Geometry = (props: GeometryProps) => {
  const { vertices, indices, normals, uvs, wireframe } = props;

  const ref = useRef<BufferGeometry>(null);

  useEffect(() => {
    if (ref.current) {
      const buffer = ref.current;

      buffer.clearGroups();

      buffer.setIndex(indices);
      buffer.setAttribute('position', new Float32BufferAttribute(vertices, 3));
      buffer.setAttribute('normal', new Float32BufferAttribute(normals, 3));
      buffer.setAttribute('uv', new Float32BufferAttribute(uvs, 2));
    }
  }, [vertices, indices, normals, uvs]);

  return (
    <mesh>
      <bufferGeometry ref={ref} />
      <meshStandardMaterial color={'red'} envMapIntensity={0.15} wireframe={wireframe} />
    </mesh>
  )
}