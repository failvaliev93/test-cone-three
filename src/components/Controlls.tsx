import { useControls } from "leva";
import { useState, useEffect } from 'react';
import { useDebounce } from '@uidotdev/usehooks';
import { Geometry } from "./Geometry";
import { calc } from "./calc";

interface GeometryProps {
  vertices: number[];
  indices: number[];
  normals: number[];
  uvs: number[];
}

export const Controlls = () => {
  const { height, radius, segments, wireframe } = useControls({
    height: {
      value: 1,
      min: 0.1,
      max: 10,
      step: 0.1,
    },
    radius: {
      value: 1,
      min: 0.1,
      max: 2,
      step: 0.1,
    },
    segments: {
      value: 4,
      min: 4,
      max: 32,
      step: 1,
    },
    wireframe: false,
  });

  const [geometryProps, setGeometryProps] = useState<GeometryProps | null>(null);

  const debouncedHeight = useDebounce(height, 300);
  const debouncedRadius = useDebounce(radius, 300);
  const debouncedSegments = useDebounce(segments, 300);

  useEffect(() => {
    const vertices: number[] = [], indices: number[] = [], normals: number[] = [], uvs: number[] = [];
    calc(vertices, indices, normals, uvs, debouncedHeight, debouncedRadius, debouncedSegments);

    setGeometryProps({
      indices,
      vertices,
      normals,
      uvs,
    })
  }, [debouncedHeight, debouncedRadius, debouncedSegments]);

  if (!geometryProps) {
    return null;
  }

  return (
    <Geometry
      wireframe={wireframe}
      indices={geometryProps.indices}
      normals={geometryProps.normals}
      uvs={geometryProps.uvs}
      vertices={geometryProps.vertices}
    />
  )
}
